# SSH Config : ssh-cfg

## Description
Small script to configure several ssh environment and merge it if needed.  
Generate some ssh environments like perso, work, dev with their own config & key list.
Activate some of them to generate a ssh config and load needed keys.  
Keep autocompletion working out of the box.

## Installation
1. Copy the ssh bash script `ssh-cfg` into your home bin dir and put it executable using this command :  
  `target_dir="${HOME}/bin" && mkdir -p "${target_dir}" && curl -s 'https://gitlab.com/thepozer/ssh-cfg/-/raw/main/ssh-cfg' -o "${target_dir}/ssh-cfg" && chmod a+x "${target_dir}/ssh-cfg"`  
  You can change the target directory by changing the content of the `target_dir` variable at the begging of the script.
2. Use it ;)

## Keep it up to date 
Use intgrated self update command : `ssh-cfg selfupdate` or `ssh-cfg self-update`

## Exemple
First things to do is to initialise it (mainly to keep a copy of your current ssh config and to import it into parts) :
```
$ ssh-cfg init
```

After that, create one or more config part and customize config and keys list :
```
$ ssh-cfg add work
$ ssh-cfg add perso
$ ssh-cfg add oss
...
```

After that you can list parts,
```
$ ssh-cfg ls
```

Enable or disable some of them : 
```
$ ssh-cfg en work
$ ssh-cfg en perso
$ ssh-cfg en oss

$ ssh-cfg dis perso
```
Each time, it will 
 1. rebuild the ssh config file by concatenate configs of selected parts
 2. clear the keys from the ssh-agent 
 3. reload keys of selected parts

Config will not be lost at reboot, just reload keys with 
```
$ ssh-cfg load
```

## Usage
```
Usage : ssh-cfg COMMAND [[SUB COMMAND] partname]

Dynamicaly change ssh configuration
ssh-agent need to be already launched

Managment commands :
  init    : Initialise ssh config to use ssh-cfg
  add     : Add a new config part (needs a partname)
  edit    : Edit elements of a config part
    List of sub commands : (all needs a partname)
      config    : Edit part ssh configuration
      desc      : Edit part description
      list-keys : List ssh keys
      add-keys  : add ssh keys
  remove  : Remove a config part (needs a partname)
  show    : Show all information about a config part (needs a partname)

Usage commands 
  load         : Load selected ssh keys into ssh-agent
  unload       : Unload selected ssh keys from ssh-agent
  clean        : Unload all ssh keys from ssh-agent
  list,ls      : list config parts and which is activated
  enable, en   : Enable a config part and load keys into ssh-agent (needs a partname)
  disable, dis : Disable a config part and load keys into ssh-agent (needs a partname)

Other commands 
  help    : Show this message

```

## Support
Tell me if something need to be improved ...


## Authors and acknowledgment
Me (Didier "thepozer" Prolhac) :) 

## License
MIT 

## Project status
Just writing it for me, because I need it
